//
//  WebRegisterView.swift
//  Education
//
//  Created by Thumar Kishan on 02/08/21.
//

import UIKit
import WebKit
import SDWebImage
import AVFoundation
import NVActivityIndicatorView
import MobileRTC

class WebRegisterView: UIViewController, WKUIDelegate, WKNavigationDelegate
{
    var Str_Urlpath = ""
    var Str_Title = ""
    var Str_School_ID = ""
    var Str_Authtoken = ""
    var Str_CheckCookies = ""
    var Str_MeetingID = ""
    
    var Str_Api_MeetingID: String? = ""
    var Str_Api_Password: String? = ""
    var Str_Zak = ""

    var Str_Rest = ""
    var Str_TutorUrl = ""
    
    var str_lastloadedurl = ""
    var timer = Timer()

    @IBOutlet weak var Webview: WKWebView!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.tintColor = UIColor.white
        let ver = UIDevice.current.systemVersion.components(separatedBy: ".")
        if Int(ver[0]) ?? 0 >= 7 {
            navigationController?.navigationBar.barTintColor = UIColor(red: 0.13, green: 0.59, blue: 0.95, alpha: 1.00)
            navigationController?.navigationBar.isTranslucent = false
        } else {
            navigationController?.navigationBar.tintColor = UIColor(red: 0.13, green: 0.59, blue: 0.95, alpha: 1.00)
        }
        self.navigationItem.setHidesBackButton(false, animated: false)

        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)

        
//        self.navigationItem.title = Str_Title

        if Str_CheckCookies == "2"
        {
            let cookie = HTTPCookie(properties: [
                .domain: "",
                .path: "",
                .name: "auth_hash",
                .value: Str_Authtoken,
                .secure: "TRUE",
                .expires: NSDate(timeIntervalSinceNow: 31556926)
            ])!
            let cookie1 = HTTPCookie(properties: [
                .domain: "",
                .path: "",
                .name: "selected_school",
                .value: Str_School_ID,
                .secure: "TRUE",
                .expires: NSDate(timeIntervalSinceNow: 31556926)
            ])!

            Webview.configuration.websiteDataStore.httpCookieStore.setCookie(cookie)
            Webview.configuration.websiteDataStore.httpCookieStore.setCookie(cookie1)
        }
        

        let url = URL(string: Str_Urlpath)
        print(Str_Urlpath)
        Webview.customUserAgent = "Mozilla/5.0 (Linux; Android 11; SM-M307F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.164 Mobile Safari/537.36;iOSAPP"
        let request = URLRequest(url: url!)
        Webview.navigationDelegate = self
        Webview.uiDelegate = self
        Webview.configuration.preferences.javaScriptEnabled = true
        Webview.load(request)
        Webview.addObserver(self, forKeyPath: "URL", options: .new, context: nil)

        Webview.configuration.userContentController.addUserScript(self.getZoomDisableScript())

        MobileRTC.shared().setMobileRTCRootController(self.navigationController)

        /// Notification that is used to start a meeting upon log in success.
        NotificationCenter.default.addObserver(self, selector: #selector(userLoggedIn), name: NSNotification.Name(rawValue: "userLoggedIn"), object: nil)


        
    }
    
    private func getZoomDisableScript() -> WKUserScript
    {
        let source: String = "var meta = document.createElement('meta');" +
            "meta.name = 'viewport';" +
            "meta.content = 'width=device-width, initial-scale=1.0, maximum- scale=1.0, user-scalable=no';" +
            "var head = document.getElementsByTagName('head')[0];" + "head.appendChild(meta);"
        return WKUserScript(source: source, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
    }

    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        navigationController?.isNavigationBarHidden = false
        
        Str_Rest = ""

        let url = URL(string: Str_Urlpath)
        print(Str_Urlpath)
        Webview.customUserAgent = "iOSAPP"
        let request = URLRequest(url: url!)
        Webview.navigationDelegate = self
        Webview.uiDelegate = self
        Webview.configuration.preferences.javaScriptEnabled = true
        Webview.load(request)
        Webview.configuration.userContentController.addUserScript(self.getZoomDisableScript())
    }
    
    
    override func viewWillDisappear(_ animated: Bool)
    {
//        Webview!.removeObserver(self, forKeyPath: "URL")
    }


    @objc func applicationDidBecomeActive(notification: NSNotification)
    {
        Str_Rest = ""

        let url = URL(string: Str_Urlpath)
        print(Str_Urlpath)
        Webview.customUserAgent = "iOSAPP"
        let request = URLRequest(url: url!)
        Webview.navigationDelegate = self
        Webview.uiDelegate = self
        Webview.configuration.preferences.javaScriptEnabled = true
        Webview.load(request)
        Webview.configuration.userContentController.addUserScript(self.getZoomDisableScript())
    }

    // Observe value
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        if let key = change?[NSKeyValueChangeKey.newKey]
        {
            print("observeValue \(key)") // url value
            let urlString: String = (key as AnyObject).absoluteString!!
            let stringConMeetingStart = urlString.contains("conference-meeting/start")
            let stringConMeetingJoin = urlString.contains("conference-meeting/join")
            Str_TutorUrl = urlString
            
            if stringConMeetingStart == true
            {
//                Str_Rest = "restricted.com"
                let fullNameArr = urlString.split(separator:"/")
                Str_MeetingID = String(fullNameArr.last!)
                GetMeeting_Data()
            }
            else if stringConMeetingJoin == true
            {
//                Str_Rest = "restricted.com"
                let fullNameArr = urlString.split(separator:"/")
                Str_MeetingID = String(fullNameArr.last!)
                GetMeeting_Data()
            }

            let stringResult = urlString.contains("zClient")
            print(stringResult)
            if stringResult == true
            {
                Str_Rest = "restricted.com"
                let url = URL(string: urlString)!
                Str_MeetingID = url["meeting"]!
                print(Str_MeetingID)
                if str_lastloadedurl !=  urlString
                {
                    GetMeeting_Zoom()
                }
            }
            
            
            str_lastloadedurl = urlString

            
        }
        else
        {
            let key = change?[NSKeyValueChangeKey.newKey]
            print("observeValue \(key ?? "")") // url value
        }
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void)
    {
        if Str_Rest == "restricted.com"
        {
          decisionHandler(.cancel)
          Str_Rest = ""
          return
        }

        decisionHandler(.allow)
    }

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Started to load")
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!)
    {
        print("Finished loading")
    }

    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }

    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping () -> Void) {

        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            completionHandler()
        }))

        present(alertController, animated: true, completion: nil)
    }


    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping (Bool) -> Void) {

        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .actionSheet)

        alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
            completionHandler(true)
        }))

        alertController.addAction(UIAlertAction(title: "No", style: .default, handler: { (action) in
            completionHandler(false)
        }))

        present(alertController, animated: true, completion: nil)
    }


    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping (String?) -> Void) {

        let alertController = UIAlertController(title: nil, message: prompt, preferredStyle: .actionSheet)

        alertController.addTextField { (textField) in
            textField.text = defaultText
        }

        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            if let text = alertController.textFields?.first?.text {
                completionHandler(text)
            } else {
                completionHandler(defaultText)
            }
        }))

        alertController.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
            completionHandler(nil)
        }))

        present(alertController, animated: true, completion: nil)
    }

    private func GetMeeting_Data()
    {
        let defaults1 = UserDefaults.standard
        let Str_Token = defaults1.object(forKey: "ValidateToken")as! String

        
        
        let parameters = ["meeting_id": Str_MeetingID, "token": Str_Token]

        let frame = CGRect(x: (self.view.bounds.width/2) - 25, y: (self.view.bounds.height/2) - 108, width: 50, height: 50)
        let activityIndicatorView = NVActivityIndicatorView(frame: frame,
                                                            type: NVActivityIndicatorType.ballSpinFadeLoader, color: UIColor.black)
        self.view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()

        APIManager.shared.GetZoomMeetingData(parameters:parameters as NSDictionary, onSuccess: { [self] (data) in
            
            guard (data.value(forKey:"success") as? NSNumber) != nil else
            {
                let Str_Type = data.value(forKey: "type")as! String
                if Str_Type == "bbb"
                {
                    
                    let Str_home_path = defaults1.object(forKey: "home_path")as! String
                    let Str_redirect_url = (data.value(forKey: "redirect_url")as? String)
                    let Str_Url123 = String(format: "%@%@?auth_token=%@", Str_home_path, Str_redirect_url!, Str_Authtoken)
                    print(Str_Url123)
                    guard let url1 = URL(string: Str_Url123) else { return }
                    UIApplication.shared.open(url1)
                }
                
                
                activityIndicatorView.stopAnimating()

             return
             }

        }) { (error) in
            
            activityIndicatorView.stopAnimating()
        }
    }
    
    private func GetMeeting_Zoom()
    {
        let defaults1 = UserDefaults.standard
        let Str_Token = defaults1.object(forKey: "ValidateToken")as! String

        
        
        let parameters = ["meeting_id": Str_MeetingID, "token": Str_Token]

        let frame = CGRect(x: (self.view.bounds.width/2) - 25, y: (self.view.bounds.height/2) - 108, width: 50, height: 50)
        let activityIndicatorView = NVActivityIndicatorView(frame: frame,
                                                            type: NVActivityIndicatorType.ballSpinFadeLoader, color: UIColor.black)
        self.view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()

        APIManager.shared.GetZoomMeetingData(parameters:parameters as NSDictionary, onSuccess: { [self] (data) in
            
            guard (data.value(forKey:"success") as? NSNumber) != nil else
            {
                let Str_Type = data.value(forKey: "type")as! String
                if Str_Type == "zoom"
                {
                    Str_Api_MeetingID = (data.value(forKey: "zoom_meeting_id")as? String)
                    if Str_Api_MeetingID == nil
                    {
                        
                    }
                    else
                    {
                        Str_Rest = "restricted.com"

                        Str_Api_Password = (data.value(forKey: "zoom_password")as? String)
                        let defaults = UserDefaults.standard
                        let Str_Usertype = defaults.object(forKey: "USER_TYPE")as! String
                        if Str_Usertype == "h"
                        {
                            Str_Zak = data.value(forKey: "zak")as! String

                        }
                        else if Str_Usertype == "t"
                        {
                            Str_Zak = data.value(forKey: "zak")as! String
                        }

                        presentJoinMeetingAlert()
                        
                    }
                }
                
                activityIndicatorView.stopAnimating()

             return
             }

        }) { (error) in
            
            activityIndicatorView.stopAnimating()
        }
    }
    

    
    func joinMeeting(meetingNumber: String, meetingPassword: String)
    {
        // Obtain the MobileRTCMeetingService from the Zoom SDK, this service can start meetings, join meetings, leave meetings, etc.
        if let meetingService = MobileRTC.shared().getMeetingService() {

            meetingService.delegate = self

            let defaults = UserDefaults.standard
            let Str_Usertype = defaults.object(forKey: "USER_TYPE")as! String
            let Str_UserID = defaults.object(forKey: "USER_ID")as! String
            let Str_UserName = defaults.object(forKey: "Username")as! String
            if "t,h".contains(Str_Usertype)
            {
                if let meetingSettings = MobileRTC.shared().getMeetingSettings()
                {
//                    meetingSettings.topBarHidden = true

                    meetingSettings.driveModeDisabled()
                    meetingSettings.meetingInviteHidden = true
                    meetingSettings.meetingShareHidden = true
                }
                let startMeetingParameters = MobileRTCMeetingStartParam4WithoutLoginUser()
                startMeetingParameters.meetingNumber = meetingNumber
                startMeetingParameters.zak = Str_Zak
                startMeetingParameters.userID = Str_UserID
                startMeetingParameters.userType = MobileRTCUserType.apiUser
                startMeetingParameters.userName = Str_UserName
                meetingService.startMeeting(with: startMeetingParameters)
                

            }
            else
            {
                if let meetingSettings = MobileRTC.shared().getMeetingSettings()
                {
                    meetingSettings.topBarHidden = true

                    meetingSettings.driveModeDisabled()
                    meetingSettings.meetingInviteHidden = true
                    meetingSettings.meetingShareHidden = true
                }

                let joinMeetingParameters = MobileRTCMeetingJoinParam()
                joinMeetingParameters.meetingNumber = meetingNumber
                joinMeetingParameters.password = meetingPassword
                joinMeetingParameters.userName = Str_UserName
                meetingService.joinMeeting(with: joinMeetingParameters)
            }
            
            
//            if Str_Usertype == "s"
//            {
//                let joinMeetingParameters = MobileRTCMeetingJoinParam()
//                joinMeetingParameters.meetingNumber = meetingNumber
//                joinMeetingParameters.password = meetingPassword
//                joinMeetingParameters.userName = Str_UserName
//                meetingService.joinMeeting(with: joinMeetingParameters)
//            }
//            else
//            {
//                let joinMeetingParameters = MobileRTCMeetingJoinParam()
//                joinMeetingParameters.meetingNumber = meetingNumber
//                joinMeetingParameters.password = meetingPassword
//                joinMeetingParameters.userName = Str_UserName
//                meetingService.joinMeeting(with: joinMeetingParameters)
//            }

            
            
            
//            meetingService.startMeeting(with: joinMeetingParameters)
        }
    }

    /// Logs user into their Zoom account using the user's Zoom email and password.
    ///
    /// Assign a MobileRTCAuthDelegate to listen to authorization events including onMobileRTCLoginReturn(_ returnValue: Int).
    ///
    /// - Parameters:
    ///   - email: The user's email address attached to their Zoom account.
    ///   - password: The user's password attached to their Zoom account.
    /// - Precondition:
    ///   - Zoom SDK must be initialized and authorized.
    func logIn(email: String, password: String) {
        // Obtain the MobileRTCAuthService from the Zoom SDK, this service can log in a Zoom user, log out a Zoom user, authorize the Zoom SDK etc.
        if let authorizationService = MobileRTC.shared().getAuthService() {
            // Call the login function in MobileRTCAuthService. This will attempt to log in the user.
            authorizationService.login(withEmail: email, password: password, rememberMe: false)
        }
    }

    /// Creates and starts a Zoom instant meeting. An instant meeting is an unscheduled meeting that begins instantly.
    ///
    /// Assign a MobileRTCMeetingServiceDelegate to listen to meeting events and start meeting status.
    ///
    /// - Precondition:
    ///   - Zoom SDK must be initialized and authorized.
    ///   - MobileRTC.shared().setMobileRTCRootController() has been called.
    ///   - User has logged into Zoom successfully.
    func startMeeting() {
        // Obtain the MobileRTCMeetingService from the Zoom SDK, this service can start meetings, join meetings, leave meetings, etc.
        if let meetingService = MobileRTC.shared().getMeetingService() {
            // Set the ViewController to be the MobileRTCMeetingServiceDelegate
            meetingService.delegate = self

            // Create a MobileRTCMeetingStartParam to provide the MobileRTCMeetingService with the necessary info to start an instant meeting.
            // In this case we will use MobileRTCMeetingStartParam4LoginlUser(), since the user has logged into Zoom.
            let startMeetingParameters = MobileRTCMeetingStartParam4LoginlUser()

            // Call the startMeeting function in MobileRTCMeetingService. The Zoom SDK will handle the UI for you, unless told otherwise.
            meetingService.startMeeting(with: startMeetingParameters)
        }
    }

    // MARK: - Convenience Alerts

    /// Creates alert for prompting the user to enter meeting number and password for joining a meeting.
    func presentJoinMeetingAlert()
    {
        self.joinMeeting(meetingNumber: Str_Api_MeetingID ?? "", meetingPassword: Str_Api_Password ?? "")
    }

    /// Creates alert for prompting the user to enter their Zoom credentials for starting a meeting.
    func presentLogInAlert() {
        let alertController = UIAlertController(title: "Log in", message: "", preferredStyle: .alert)

        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Email"
            textField.keyboardType = .emailAddress
        }
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Password"
            textField.keyboardType = .asciiCapable
            textField.isSecureTextEntry = true
        }

        let logInAction = UIAlertAction(title: "Log in", style: .default, handler: { alert -> Void in
            let emailTextField = alertController.textFields![0] as UITextField
            let passwordTextField = alertController.textFields![1] as UITextField

            if let email = emailTextField.text, let password = passwordTextField.text {
                self.logIn(email: email, password: password)
            }
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { (action : UIAlertAction!) -> Void in })

        alertController.addAction(logInAction)
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
    }

    // MARK: - Internal

    /// Selector that is used to start a meeting upon log in success.
    @objc func userLoggedIn() {
        startMeeting()
    }

    
}

extension URL
{
    subscript(queryParam: String) -> String? {
        guard let url = URLComponents(string: self.absoluteString) else { return nil }
        if let parameters = url.queryItems {
            return parameters.first(where: { $0.name == queryParam })?.value
        } else if let paramPairs = url.fragment?.components(separatedBy: "?").last?.components(separatedBy: "&") {
            for pair in paramPairs where pair.contains(queryParam) {
                return pair.components(separatedBy: "=").last
            }
            return nil
        } else {
            return nil
        }
    }
}

// MARK: - MobileRTCMeetingServiceDelegate

// Conform ViewController to MobileRTCMeetingServiceDelegate.
// MobileRTCMeetingServiceDelegate listens to updates about meetings, such as meeting state changes, join attempt status, meeting errors, etc.
extension WebRegisterView: MobileRTCMeetingServiceDelegate
{

    // Is called upon in-meeting errors, join meeting errors, start meeting errors, meeting connection errors, etc.
    func onMeetingError(_ error: MobileRTCMeetError, message: String?) {
        switch error {
        case .success:
            print("Successful meeting operation.")
        case .passwordError:
            print("Could not join or start meeting because the meeting password was incorrect.")
        default:
            print("MobileRTCMeetError: \(error) \(message ?? "")")
        }
    }

    // Is called when the user joins a meeting.
    func onJoinMeetingConfirmed()
    {
        print("Join meeting confirmed.")
    }

    // Is called upon meeting state changes.
    func onMeetingStateChange(_ state: MobileRTCMeetingState)
    {
        if state == MobileRTCMeetingState.inMeeting
        {
            timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        }
        else
        {
            timer.invalidate()
        }
        
        print("Current meeting state: \(state)")
    }

    @objc func timerAction()
    {
        let defaults1 = UserDefaults.standard
        let Str_UserID = defaults1.object(forKey: "USER_ID")as! String

        let parameters = ["meeting": Str_MeetingID, "user": Str_UserID]

        APIManager.shared.GetMeetingAttandanceData(parameters:parameters as NSDictionary, onSuccess: { (data) in
            
            guard (data.value(forKey:"success") as? NSNumber) != nil else
            {

                print(data)

             return
             }

        }) { (error) in
            
        }
    }

}

