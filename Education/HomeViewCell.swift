//
//  HomeViewCell.swift
//  Education
//
//  Created by Thumar Kishan on 02/08/21.
//

import UIKit

class HomeViewCell: UICollectionViewCell
{
    @IBOutlet weak var View_Round: UIView!
    
    @IBOutlet weak var Imageview_Icon: UIImageView!
    @IBOutlet weak var Lbl_Name: UILabel!
    
}
