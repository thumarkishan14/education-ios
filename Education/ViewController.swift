//
//  ViewController.swift
//  Education
//
//  Created by Thumar Kishan on 30/07/21.
//

import UIKit
import SDWebImage
import AVFoundation
import NVActivityIndicatorView
import SwiftGifOrigin
import BiometricAuthentication

class ViewController: UIViewController, UITextFieldDelegate
{

    @IBOutlet weak var View_Login: UIView!
    @IBOutlet weak var View_Password: UIView!
    
    
    @IBOutlet weak var Textfield_LoginID: UITextField!
    @IBOutlet weak var Textfield_Password: UITextField!
    
    @IBOutlet weak var Btn_Eye: UIButton!
    @IBOutlet weak var Btn_Login: UIButton!
    @IBOutlet weak var Btn_Consultation: UIButton!
    @IBOutlet weak var Btn_Register: UIButton!
    
    var Title_H_Array:[Dictionary<String, Any>] = []
    
    var Route_Array:NSMutableArray = NSMutableArray()
    var Label_Array:NSMutableArray = NSMutableArray()
    var Icon_Array:NSMutableArray = NSMutableArray()

    
    @IBOutlet weak var View_Gif: UIView!
    @IBOutlet weak var Imageview_Gif: UIImageView!
    
    var Str_BtnCheck = ""
    var Str_School_ID = ""
    var Str_CheckLoader = ""

    var Str_consultation_text = ""
    var Str_consultation_url = ""
    
    var Str_register_text = ""
    var Str_register_url = ""

    var Str_H = ""
    var Str_T = ""
    var Str_S = ""
    var Str_C = ""

    
    @IBOutlet weak var Lbl_versionlbl1: UILabel!
    @IBOutlet weak var Lbl_versionlbl2: UILabel!
  
    var Str_CheckAppidToken = ""
    
    var Str_Homepath = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]
        navigationController?.isNavigationBarHidden = true

        let defaults1 = UserDefaults.standard
        let Str_Finger = defaults1.object(forKey:"Finger_On_Off")
        if Str_Finger == nil
        {
            self.revealViewController()?.gestureEnabled = false

            self.Imageview_Gif.image = UIImage.gif(name:"loader3")

            let defaults1 = UserDefaults.standard
            let Str_UserID = defaults1.object(forKey: "USER_ID")

            if Str_UserID != nil
            {
                if Str_UserID as! String == ""
                {
                    self.Str_BtnCheck = "1"
                    self.Str_CheckAppidToken = "1"
                    
                    self.View_Login.layer.cornerRadius = 5; // this value vary as per your desire
                    self.View_Login.clipsToBounds = true;
                    self.View_Login.layer.borderWidth = 1
                    self.View_Login.layer.borderColor = UIColor.black.cgColor

                    self.View_Password.layer.cornerRadius = 5; // this value vary as per your desire
                    self.View_Password.clipsToBounds = true;
                    self.View_Password.layer.borderWidth = 1
                    self.View_Password.layer.borderColor = UIColor.black.cgColor

                    self.Btn_Login.layer.cornerRadius = 5; // this value vary as per your desire
                    self.Btn_Login.clipsToBounds = true;

                    self.Btn_Consultation.layer.cornerRadius = 5; // this value vary as per your desire
                    self.Btn_Consultation.clipsToBounds = true;

                    self.Btn_Register.layer.cornerRadius = 5; // this value vary as per your desire
                    self.Btn_Register.clipsToBounds = true;

                    self.GetAppID_Data()
                }
                else
                {
                    self.Str_CheckAppidToken="2"
                    self.GetAppID_Data()
                }
            }
            else
            {
                self.Str_BtnCheck = "1"

                self.View_Login.layer.cornerRadius = 5; // this value vary as per your desireself.
                self.View_Login.clipsToBounds = true;
                self.View_Login.layer.borderWidth = 1
                self.View_Login.layer.borderColor = UIColor.black.cgColor

                self.View_Password.layer.cornerRadius = 5; // this value vary as per your desire
                self.View_Password.clipsToBounds = true;
                self.View_Password.layer.borderWidth = 1
                self.View_Password.layer.borderColor = UIColor.black.cgColor

                self.Btn_Login.layer.cornerRadius = 5; // this value vary as per your desire
                self.Btn_Login.clipsToBounds = true;

                self.Btn_Consultation.layer.cornerRadius = 5; // this value vary as per your desire
                self.Btn_Consultation.clipsToBounds = true;

                self.Btn_Register.layer.cornerRadius = 5; // this value vary as per your desire
                self.Btn_Register.clipsToBounds = true;

                self.GetAppID_Data()
                
            }

        }
        else
        {
            if Str_Finger as! String == "on"
            {
                // Set AllowableReuseDuration in seconds to bypass the authentication when user has just unlocked the device with biometric
                BioMetricAuthenticator.shared.allowableReuseDuration = 30
                
                // start authentication
                BioMetricAuthenticator.authenticateWithBioMetrics(reason: "") { [weak self] (result) in
                        
                    switch result {
                    case .success( _):
                        
                        // authentication successful
                        self?.showLoginSucessAlert()
                        
                    case .failure(let error):
                        
                        switch error {
                            
                        // device does not support biometric (face id or touch id) authentication
                        case .biometryNotAvailable:
                            self?.showErrorAlert(message: error.message())
                            
                        // No biometry enrolled in this device, ask user to register fingerprint or face
                        case .biometryNotEnrolled:
                            self?.showGotoSettingsAlert(message: error.message())
                            
                        // show alternatives on fallback button clicked
                        case .fallback:
    //                        self?.txtUsername.becomeFirstResponder() // enter username password manually
                            break
                            // Biometry is locked out now, because there were too many failed attempts.
                        // Need to enter device passcode to unlock.
                        case .biometryLockedout:
                            self?.showPasscodeAuthentication(message: error.message())
                            
                        // do nothing on canceled by system or user
                        case .canceledBySystem, .canceledByUser:
                            break
                            
                        // show error for any other reason
                        default:
                            self?.showErrorAlert(message: error.message())
                        }
                    }
                }
                
                BioMetricAuthenticator.authenticateWithPasscode(reason: "enter password") { (result) in
                    switch result {
                    case .success( _):
                        
                        self.revealViewController()?.gestureEnabled = false

                        self.Imageview_Gif.image = UIImage.gif(name:"loader3")

                        let defaults1 = UserDefaults.standard
                        let Str_UserID = defaults1.object(forKey: "USER_ID")

                        if Str_UserID != nil
                        {
                            if Str_UserID as! String == ""
                            {
                                self.Str_BtnCheck = "1"
                                self.Str_CheckAppidToken = "1"
                                
                                self.View_Login.layer.cornerRadius = 5; // this value vary as per your desire
                                self.View_Login.clipsToBounds = true;
                                self.View_Login.layer.borderWidth = 1
                                self.View_Login.layer.borderColor = UIColor.black.cgColor

                                self.View_Password.layer.cornerRadius = 5; // this value vary as per your desire
                                self.View_Password.clipsToBounds = true;
                                self.View_Password.layer.borderWidth = 1
                                self.View_Password.layer.borderColor = UIColor.black.cgColor

                                self.Btn_Login.layer.cornerRadius = 5; // this value vary as per your desire
                                self.Btn_Login.clipsToBounds = true;

                                self.Btn_Consultation.layer.cornerRadius = 5; // this value vary as per your desire
                                self.Btn_Consultation.clipsToBounds = true;

                                self.Btn_Register.layer.cornerRadius = 5; // this value vary as per your desire
                                self.Btn_Register.clipsToBounds = true;

                                self.GetAppID_Data()
                            }
                            else
                            {
                                self.Str_CheckAppidToken="2"
                                self.GetAppID_Data()
                            }
                        }
                        else
                        {
                            self.Str_BtnCheck = "1"

                            self.View_Login.layer.cornerRadius = 5; // this value vary as per your desireself.
                            self.View_Login.clipsToBounds = true;
                            self.View_Login.layer.borderWidth = 1
                            self.View_Login.layer.borderColor = UIColor.black.cgColor

                            self.View_Password.layer.cornerRadius = 5; // this value vary as per your desire
                            self.View_Password.clipsToBounds = true;
                            self.View_Password.layer.borderWidth = 1
                            self.View_Password.layer.borderColor = UIColor.black.cgColor

                            self.Btn_Login.layer.cornerRadius = 5; // this value vary as per your desire
                            self.Btn_Login.clipsToBounds = true;

                            self.Btn_Consultation.layer.cornerRadius = 5; // this value vary as per your desire
                            self.Btn_Consultation.clipsToBounds = true;

                            self.Btn_Register.layer.cornerRadius = 5; // this value vary as per your desire
                            self.Btn_Register.clipsToBounds = true;

                            self.GetAppID_Data()
                            
                        }
                    case .failure(let error):
                        print(error.message())
                    }
                }

            }
            else
            {
                self.revealViewController()?.gestureEnabled = false
                self.Imageview_Gif.image = UIImage.gif(name:"loader3")

                let defaults1 = UserDefaults.standard
                let Str_UserID = defaults1.object(forKey: "USER_ID")


                if Str_UserID as! String == ""
                {
                    self.Str_BtnCheck = "1"
                    self.Str_CheckAppidToken = "1"
                    
                    self.View_Login.layer.cornerRadius = 5; // this value vary as per your desire
                    self.View_Login.clipsToBounds = true;
                    self.View_Login.layer.borderWidth = 1
                    self.View_Login.layer.borderColor = UIColor.black.cgColor

                    self.View_Password.layer.cornerRadius = 5; // this value vary as per your desire
                    self.View_Password.clipsToBounds = true;
                    self.View_Password.layer.borderWidth = 1
                    self.View_Password.layer.borderColor = UIColor.black.cgColor

                    self.Btn_Login.layer.cornerRadius = 5; // this value vary as per your desire
                    self.Btn_Login.clipsToBounds = true;

                    self.Btn_Consultation.layer.cornerRadius = 5; // this value vary as per your desire
                    self.Btn_Consultation.clipsToBounds = true;

                    self.Btn_Register.layer.cornerRadius = 5; // this value vary as per your desire
                    self.Btn_Register.clipsToBounds = true;

                    self.GetAppID_Data()
                }
                else
                {
                    self.Str_CheckAppidToken="2"
                    self.GetAppID_Data()
                }
            }
        }
        
        Textfield_LoginID.attributedPlaceholder = NSAttributedString(string: "Login ID", attributes: [NSAttributedString.Key.foregroundColor : UIColor.darkGray])
        Textfield_Password.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor : UIColor.darkGray])


    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]
        navigationController?.isNavigationBarHidden = true

    }

    // show passcode authentication
    func showPasscodeAuthentication(message: String)
    {
        BioMetricAuthenticator.authenticateWithPasscode(reason: message) { [weak self] (result) in
            switch result {
            case .success( _):
                self?.showLoginSucessAlert() // passcode authentication success
            case .failure(let error):
                print(error.message())
            }
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.view.endEditing(true)
        return false
    }

    @IBAction func Eye_Clicked(_ sender: Any)
    {
        if Str_BtnCheck == "1"
        {
            Str_BtnCheck = "2"
            Btn_Eye.setImage(UIImage(named: "hidden.png"), for: .normal)
            Textfield_Password.isSecureTextEntry = false
            Textfield_Password.clearsOnInsertion = false
            Textfield_Password.clearsOnBeginEditing = false
        }
        else
        {
            Str_BtnCheck = "1"
            Btn_Eye.setImage(UIImage(named: "eye.png"), for: .normal)
            Textfield_Password.isSecureTextEntry = true
            Textfield_Password.clearsOnInsertion = false
            Textfield_Password.clearsOnBeginEditing = false
        }
        
    }
    
    @IBAction func Login_CLicked(_ sender: Any)
    {
        UserDefaults.standard.set("", forKey: "USER_TYPE")
        UserDefaults.standard.set("", forKey: "USER_ID")
        UserDefaults.standard.set("", forKey: "USER_TOKEN_EXPIRES")
        UserDefaults.standard.set("", forKey: "USER_TOKEN")
        UserDefaults.standard.set("", forKey: "ValidateToken")
        UserDefaults.standard.set("", forKey: "Route_Array")
        UserDefaults.standard.set("", forKey: "Icon_Array")
        UserDefaults.standard.set("", forKey: "Label_Array")
        UserDefaults.standard.set("", forKey: "Finger_On_Off")

        GetAppToken_Data()
    }

    @IBAction func Consulation_Clicked(_ sender: Any)
    {
        let detail = storyboard?.instantiateViewController(withIdentifier: "WebRegisterView") as! WebRegisterView
        detail.Str_Urlpath = String(format: "%@%@", Str_Homepath, Str_consultation_url)
        detail.Str_Title = "Consulation"
        detail.Str_CheckCookies = "1"
        self.navigationController?.pushViewController(detail, animated: true)
    }
    
    @IBAction func Register_Clicked(_ sender: Any)
    {
        let detail = storyboard?.instantiateViewController(withIdentifier: "WebRegisterView") as! WebRegisterView
        detail.Str_Urlpath = String(format: "%@", Str_register_url)
        detail.Str_Title = "Register"
        detail.Str_CheckCookies = "1"
        self.navigationController?.pushViewController(detail, animated: true)
    }
    
    private func GetAppID_Data()
    {
        let parameters = ["": ""]
        

        APIManager.shared.GetAppIDdata(parameters:parameters as NSDictionary, onSuccess: { (data) in

           guard (data.value(forKey:"success") as? NSNumber) != nil else
           {
            
            let Str_APipath = data.value(forKey: "api_path")as? String
            self.Str_Homepath = data.value(forKey: "home_path")as! String
            self.Str_School_ID = data.value(forKey: "school_id")as! String
            let Str_show_app_banner_for = data.value(forKey: "show_app_banner_for")as! String
            let Str_VersionID = data.value(forKey: "id")as! String

            UserDefaults.standard.set(Str_APipath, forKey: "api_path")
            UserDefaults.standard.set(self.Str_Homepath, forKey: "home_path")
            UserDefaults.standard.set(self.Str_School_ID, forKey: "school_id")
            UserDefaults.standard.set(Str_show_app_banner_for, forKey: "show_app_banner_for")
            UserDefaults.standard.set(Str_VersionID, forKey: "version_id")

            
            
            self.Lbl_versionlbl1.text = String(format: "ETO Client v4.0 build 400/%@", Str_VersionID)
            self.Lbl_versionlbl2.text = String(format: "ETO Client v4.0 build 400/%@", Str_VersionID)

            
            
            
            self.Str_consultation_text = data.value(forKey: "consultation_text")as! String
            if self.Str_consultation_text == ""
            {
                self.Btn_Consultation.isHidden = true
            }
            else
            {
                self.Btn_Consultation.isHidden = false
                self.Str_consultation_url = data.value(forKey: "consultation_url") as! String
            }
            
            self.Str_register_text = data.value(forKey: "reg_text")as! String
            if self.Str_register_text == ""
            {
                self.Btn_Register.isHidden = true
            }
            else
            {
                self.Btn_Register.isHidden = false
                self.Str_register_url = data.value(forKey: "reg_url") as! String
            }

            self.Str_H = data.value(forKey: "tiles_h")as! String
            self.Str_T = data.value(forKey: "tiles_t")as! String
            self.Str_S = data.value(forKey: "tiles_s")as! String
            self.Str_C = data.value(forKey: "tiles_c")as! String

            if self.Str_CheckAppidToken == "2"
            {
                self.GetApiToken_Data()
            }
            else
            {
                if self.Str_CheckLoader == "2"
                {
                    self.View_Gif.isHidden = true
                }
                else
                {
                    _ = Timer.scheduledTimer(withTimeInterval: 5, repeats: false) { timer in
                        self.View_Gif.isHidden = true
                    }
                }
            }
            
            
            
            return
            }

        }) { (error) in
        }
    }

    func fillData_Education(dic:NSDictionary)
    {
        self.Route_Array.add(dic.value(forKey:"route") as! String)
        self.Label_Array.add(dic.value(forKey:"label") as! String)
        self.Icon_Array.add(dic.value(forKey:"icon") as! String)
    }

    private func GetAppToken_Data()
    {
        let str_username = Textfield_LoginID.text
        let str_password = Textfield_Password.text

        if str_username == ""
        {
            let alert = UIAlertController(title: "Alert", message: "Please enter username.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                    case .default:
                    print("default")
                    
                    case .cancel:
                    print("cancel")
                    
                    case .destructive:
                    print("destructive")
                    
                }
            }))
            self.present(alert, animated: true, completion: nil)
            
            return
        }
        
        if str_password == ""
        {
            let alert = UIAlertController(title: "Alert", message: "Please enter password.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                    case .default:
                    print("default")
                    
                    case .cancel:
                    print("cancel")
                    
                    case .destructive:
                    print("destructive")
                    
                }
            }))
            self.present(alert, animated: true, completion: nil)
            
            return
        }

        

        let parameters = ["username": str_username, "password": str_password, "school": Str_School_ID]

        let frame = CGRect(x: (self.view.bounds.width/2) - 25, y: (self.view.bounds.height/2) - 108, width: 50, height: 50)
        let activityIndicatorView = NVActivityIndicatorView(frame: frame,
                                                            type: NVActivityIndicatorType.ballSpinFadeLoader, color: UIColor.black)
        self.view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()

        APIManager.shared.GetAppToken(parameters:parameters as NSDictionary, onSuccess: { [self] (data) in
            
            if data=="auth_failed"
            {
                let alert = UIAlertController(title: "Error", message: "The username/password provided is incorrect.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    switch action.style{
                        case .default:
                        print("default")
                        
                        case .cancel:
                        print("cancel")
                        
                        case .destructive:
                        print("destructive")
                    }
                }))
                self.present(alert, animated: true, completion: nil)
                
                activityIndicatorView.stopAnimating()
            }
            else
            {
                UserDefaults.standard.set(data, forKey: "ValidateToken")

                let base64Encoded = data
                let decodedData = Data(base64Encoded: base64Encoded)!
                let decodedString = String(data: decodedData, encoding: .utf8)!
               
                let fullNameArr = decodedString.split(separator:"|")
                
                let Usertype = String(fullNameArr[0])
                let UserID = String(fullNameArr[1])
                let UserTokenExpire = String(fullNameArr[2])
                let UserToken = String(fullNameArr[3])

                
                UserDefaults.standard.set(Usertype, forKey: "USER_TYPE")
                UserDefaults.standard.set(UserID, forKey: "USER_ID")
                UserDefaults.standard.set(UserTokenExpire, forKey: "USER_TOKEN_EXPIRES")
                UserDefaults.standard.set(UserToken, forKey: "USER_TOKEN")
                

                print(decodedString)
                activityIndicatorView.stopAnimating()
                
                let detail = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                detail.Str_Check = "2"
                if Usertype == "h"
                {
                    guard let jsonString = self.Str_H.data(using: .utf8, allowLossyConversion: false) else { return }
                    self.Title_H_Array = try! JSONSerialization.jsonObject(with: jsonString, options: .mutableContainers) as! [Dictionary<String, Any>]
                }
                else if Usertype == "t"
                {
                    guard let jsonString = self.Str_T.data(using: .utf8, allowLossyConversion: false) else { return }
                    self.Title_H_Array = try! JSONSerialization.jsonObject(with: jsonString, options: .mutableContainers) as! [Dictionary<String, Any>]
                }
                else if Usertype == "s"
                {
                    guard let jsonString = self.Str_S.data(using: .utf8, allowLossyConversion: false) else { return }
                    self.Title_H_Array = try! JSONSerialization.jsonObject(with: jsonString, options: .mutableContainers) as! [Dictionary<String, Any>]
                }
                else
                {
                    guard let jsonString = self.Str_C.data(using: .utf8, allowLossyConversion: false) else { return }
                    self.Title_H_Array = try! JSONSerialization.jsonObject(with: jsonString, options: .mutableContainers) as! [Dictionary<String, Any>]
                }
                for index in 0..<self.Title_H_Array.count
                {
                    let dictData3 = self.Title_H_Array[index]
                    let str_route : String = dictData3["route"] as! String
                    let str_label : String = dictData3["label"] as! String
                    let str_icon : String = dictData3["icon"] as! String

                    self.Route_Array.add(str_route)
                    self.Label_Array.add(str_label)
                    self.Icon_Array.add(str_icon)
                }

                print(self.Route_Array)
                print(self.Label_Array)
                print(self.Icon_Array)

                
                let defaults = UserDefaults.standard
                defaults.set(self.Route_Array, forKey: "Route_Array")
                defaults.set(self.Icon_Array, forKey: "Icon_Array")
                defaults.set(self.Label_Array, forKey: "Label_Array")
              
                detail.Str_CheckLoader = "1"

                self.navigationController?.pushViewController(detail, animated: true)
            }

        }) { (error) in
            let alert = UIAlertController(title: "Error", message: "The username/password provided is incorrect.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                    case .default:
                    print("default")
                    
                    case .cancel:
                    print("cancel")
                    
                    case .destructive:
                    print("destructive")
                }
            }))
            self.present(alert, animated: true, completion: nil)
            
            activityIndicatorView.stopAnimating()
        }
    }
    
    private func GetApiToken_Data()
    {
        let defaults1 = UserDefaults.standard
        let Str_UserToken = defaults1.object(forKey: "ValidateToken")
        
        let parameters = ["token": Str_UserToken]

        APIManager.shared.GetValidateToken(parameters:parameters as NSDictionary, onSuccess: { [self] (data) in
            
            if data=="auth_failed"
            {
            }
            else
            {
                print(data)

                
                if data == "valid"
                {
                    if self.Str_CheckLoader == "2"
                    {
                        self.View_Gif.isHidden = true
                    }
                    else
                    {
                        _ = Timer.scheduledTimer(withTimeInterval: 5, repeats: false) { timer in
                            self.View_Gif.isHidden = true
                        }
                    }

                    let detail = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    detail.Str_Check = "1"
                    self.navigationController?.pushViewController(detail, animated: false)
                }
                else
                {
                    UserDefaults.standard.set("", forKey: "USER_TYPE")
                    UserDefaults.standard.set("", forKey: "USER_ID")
                    UserDefaults.standard.set("", forKey: "USER_TOKEN_EXPIRES")
                    UserDefaults.standard.set("", forKey: "USER_TOKEN")
                    UserDefaults.standard.set("", forKey: "ValidateToken")
                    
                    let detail = storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                    detail.Str_CheckLoader = "2"
                    self.navigationController?.pushViewController(detail, animated: true)
                }
                
            }

        }) { (error) in
        }
    }
}

// MARK: - Alerts
extension ViewController {
    
    func showAlert(title: String, message: String) {
        
        let okAction = AlertAction(title: OKTitle)
        let alertController = getAlertViewController(type: .alert, with: title, message: message, actions: [okAction], showCancel: false) { (button) in
        }
        present(alertController, animated: true, completion: nil)
    }
    
    func showLoginSucessAlert() {
//        showAlert(title: "Success", message: "Login successful")
    }
    
    func showErrorAlert(message: String) {
        showAlert(title: "Error", message: message)
    }
    
    func showGotoSettingsAlert(message: String) {
        let settingsAction = AlertAction(title: "Go to settings")
        
        let alertController = getAlertViewController(type: .alert, with: "Error", message: message, actions: [settingsAction], showCancel: true, actionHandler: { (buttonText) in
            if buttonText == CancelTitle { return }
            
            // open settings
            let url = URL(string: UIApplication.openSettingsURLString)
            if UIApplication.shared.canOpenURL(url!) {
                UIApplication.shared.open(url!, options: [:])
            }
            
        })
        present(alertController, animated: true, completion: nil)
    }
}
