//
//  SideMenuViewController.swift
//  CustomSideMenuiOSExample
//
//  Created by John Codeos on 2/7/21.
//

import UIKit
import SDWebImage
import AVFoundation
import NVActivityIndicatorView

protocol SideMenuViewControllerDelegate {
    func selectedCell(_ row: Int)
}

class SideMenuViewController: UIViewController {
    @IBOutlet var headerImageView: UIImageView!
    @IBOutlet var sideMenuTableView: UITableView!
    @IBOutlet var footerLabel: UILabel!

    var delegate: SideMenuViewControllerDelegate?

    var defaultHighlightedCell: Int = 0

    var menu: [SideMenuModel] = [
        SideMenuModel(title: "Change Password"),
        SideMenuModel(title: "Enable fingerprint authentication"),
        SideMenuModel(title: "Logout")
    ]

    @IBOutlet weak var Image_Profile: UIImageView!
    
    @IBOutlet weak var Lbl_Name: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(loadList), name: NSNotification.Name(rawValue: "Sidemenu_Select"), object: nil)

        Image_Profile.layer.cornerRadius = Image_Profile.frame.size.height/2;         Image_Profile.clipsToBounds = true;

        
        // TableView
        self.sideMenuTableView.delegate = self
        self.sideMenuTableView.dataSource = self
        self.sideMenuTableView.backgroundColor = UIColor.white
        self.sideMenuTableView.separatorStyle = .none

        // Set Highlighted Cell
        DispatchQueue.main.async {
            let defaultRow = IndexPath(row: self.defaultHighlightedCell, section: 0)
            self.sideMenuTableView.selectRow(at: defaultRow, animated: false, scrollPosition: .none)
        }

        // Footer
//        self.footerLabel.textColor = UIColor.white
//        self.footerLabel.font = UIFont.systemFont(ofSize: 12, weight: .bold)
//        self.footerLabel.text = "Developed by John Codeos"

        // Register TableView Cell
        self.sideMenuTableView.register(SideMenuCell.nib, forCellReuseIdentifier: SideMenuCell.identifier)

        // Update TableView with the data
        self.sideMenuTableView.reloadData()
    }
    
    @objc func loadList(notification: NSNotification)
    {
        let defaults1 = UserDefaults.standard
        let Str_Username = defaults1.object(forKey: "Username")
        let Str_UserImage = defaults1.object(forKey: "UserImage")

        Lbl_Name.text = Str_Username as? String
        
        if Str_UserImage as! String == "/res/default.png"
        {
            
        }
        else
        {
            Image_Profile.sd_imageIndicator = SDWebImageActivityIndicator.gray
            Image_Profile.sd_setImage(with: URL(string: Str_UserImage as! String), placeholderImage: nil)
        }
    }

}


// MARK: - UITableViewDelegate

extension SideMenuViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

// MARK: - UITableViewDataSource

extension SideMenuViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menu.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SideMenuCell.identifier, for: indexPath) as? SideMenuCell else { fatalError("xib doesn't exist") }

//        cell.iconImageView.image = self.menu[indexPath.row].icon
        cell.titleLabel.text = self.menu[indexPath.row].title
//        cell.Lbl_Description.text = self.menu[indexPath.row].description

        if indexPath.row == 0
        {
            cell.Switch.isHidden = true
        }
        else if indexPath.row == 1
        {
            let defaults1 = UserDefaults.standard
            let Str_Finger = defaults1.object(forKey:"Finger_On_Off")
            if Str_Finger == nil
            {
                cell.Switch.isOn = false
            }
            else
            {
                if Str_Finger as! String == "on"
                {
                    cell.Switch.isOn = true
                }
                else
                {
                    cell.Switch.isOn = false
                }
            }

            cell.Switch.isHidden = false
            cell.Switch.addTarget(self, action: #selector(switchValueDidChange(_:)), for: .valueChanged)
        }
        else if indexPath.row == 2
        {
            cell.Switch.isHidden = true
        }


        // Highlighted color
        let myCustomSelectionColorView = UIView()
        myCustomSelectionColorView.backgroundColor = UIColor.white
        cell.selectedBackgroundView = myCustomSelectionColorView
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.selectedCell(indexPath.row)
        // Remove highlighted color when you press the 'Profile' and 'Like us on facebook' cell
        if indexPath.row == 4 || indexPath.row == 6 {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    @objc func switchValueDidChange(_ sender: UISwitch!)
    {
        let defaults1 = UserDefaults.standard
        let Str_Finger = defaults1.object(forKey:"Finger_On_Off")
        if Str_Finger == nil
        {
            if (sender.isOn)
            {
                UserDefaults.standard.set("on", forKey: "Finger_On_Off")
            }
        }
        else
        {
            if (sender.isOn)
            {
                UserDefaults.standard.set("on", forKey: "Finger_On_Off")
            }
            else
            {
                UserDefaults.standard.set("off", forKey: "Finger_On_Off")
            }
        }

    }

}
