//
//  HomeViewController.swift
//  Education
//
//  Created by Thumar Kishan on 30/07/21.
//

import UIKit
import SDWebImage
import AVFoundation
import NVActivityIndicatorView
import SwiftGifOrigin
import WebKit
//import MobileRTC

class HomeViewController: UIViewController, WKNavigationDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, WKUIDelegate

{

    @IBOutlet weak var View_NameBorder: UIView!
    @IBOutlet weak var View_Border_Height: NSLayoutConstraint!
    
    @IBOutlet weak var View_First: UIView!
    @IBOutlet weak var View_Second: UIView!
    @IBOutlet weak var View_Third: UIView!
    @IBOutlet weak var View_Four: UIView!
    
    @IBOutlet weak var View_Gif: UIView!
    @IBOutlet weak var Imageview_Gif: UIImageView!

    var Str_Check = ""
    var Str_Title_Zoom = ""
    var Str_CheckLoader = ""
    var Str_Encodestring = ""

    var Route_Array:NSMutableArray = NSMutableArray()
    var Label_Array:NSMutableArray = NSMutableArray()
    var Icon_Array:NSMutableArray = NSMutableArray()

    @IBOutlet weak var Collectioview_home: UICollectionView!
    
    @IBOutlet weak var WebView: WKWebView!
    
    @IBOutlet weak var Lbl_versionlbl1: UILabel!
    @IBOutlet weak var Lbl_versionlbl2: UILabel!
   
    @IBOutlet weak var Btn_Menu: UIBarButtonItem!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]
        navigationController?.isNavigationBarHidden = true


        NotificationCenter.default.addObserver(self, selector: #selector(GetApiToken_Data), name: NSNotification.Name(rawValue: "GetAPItoken"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(Logout), name: NSNotification.Name(rawValue: "Logout"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(ChangePassword), name: NSNotification.Name(rawValue: "Change_Password"), object: nil)

        let defaults = UserDefaults.standard
        let Str_routearray = defaults.object(forKey: "Route_Array")
        let Str_iconarray = defaults.object(forKey: "Icon_Array")
        let Str_labelarray = defaults.object(forKey: "Label_Array")


        self.Route_Array = (Str_routearray as! NSArray).mutableCopy() as! NSMutableArray
        self.Icon_Array = (Str_iconarray as! NSArray).mutableCopy() as! NSMutableArray
        self.Label_Array = (Str_labelarray as! NSArray).mutableCopy() as! NSMutableArray

        let Str_version_id = defaults.object(forKey: "version_id")as! String
        self.Lbl_versionlbl1.text = String(format: "ETO Client v4.0 build 400/%@", Str_version_id)
        self.Lbl_versionlbl2.text = String(format: "ETO Client v4.0 build 400/%@", Str_version_id)

        let Str_show_app_banner_for = defaults.object(forKey: "show_app_banner_for")as! String
        let Str_Usertype = defaults.object(forKey: "USER_TYPE")as! String
        if Str_show_app_banner_for.contains(Str_Usertype)
        {
        }
        else
        {
            View_NameBorder.frame.size.height = 0
            View_Border_Height.constant = 0
        }

        
        
        
        Collectioview_home.delegate = self
        Collectioview_home.dataSource = self

        Imageview_Gif.image = UIImage.gif(name:"loader3")


        let defaults1 = UserDefaults.standard

        let Str_UserType = defaults1.object(forKey: "USER_ID")
        let Str_UserTokenExpire = defaults1.object(forKey: "USER_TOKEN")
        let Str_DecodeStr = String(format: "%@:%@", Str_UserType! as! CVarArg, Str_UserTokenExpire! as! CVarArg)
        let utf8str = Str_DecodeStr.data(using: .utf8)
        if let base64Encoded = utf8str?.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
        {
            Str_Encodestring = base64Encoded
            print("Encoded: \(base64Encoded)")
        }
        let Str_SchoolID = defaults1.object(forKey: "school_id")as! String

        let cookie = HTTPCookie(properties: [
            .domain: "",
            .path: "",
            .name: "auth_hash",
            .value: Str_Encodestring,
            .secure: "TRUE",
            .expires: NSDate(timeIntervalSinceNow: 31556926)
        ])!
        let cookie1 = HTTPCookie(properties: [
            .domain: "",
            .path: "",
            .name: "selected_school",
            .value: Str_SchoolID,
            .secure: "TRUE",
            .expires: NSDate(timeIntervalSinceNow: 31556926)
        ])!

        let Str_WebviewLink1 = defaults1.object(forKey: "home_path")as? String
        let Str_WebviewLink2 = String(format: "/MobileApp/dashboardBanner?auth_token=%@",Str_Encodestring as CVarArg)
        let Str_WebviewLink = Str_WebviewLink1! + Str_WebviewLink2

        WebView.configuration.websiteDataStore.httpCookieStore.setCookie(cookie)
        WebView.configuration.websiteDataStore.httpCookieStore.setCookie(cookie1)
        
        WebView.customUserAgent = "iOSAPP"
        let url = URL(string: Str_WebviewLink)
        let request = URLRequest(url: url!)
        WebView.navigationDelegate = self
        WebView.uiDelegate = self
        WebView.configuration.preferences.javaScriptEnabled = true
        WebView.load(request)
        WebView.addObserver(self, forKeyPath: "URL", options: .new, context: nil)


        
        GetProfileInfo_Data()
        

        if Str_Check == "1"
        {
            GetApiToken_Data1()
        }

        Btn_Menu.target = revealViewController()
        Btn_Menu.action = #selector(revealViewController()?.revealSideMenu)
        self.revealViewController()?.gestureEnabled = false


//        MobileRTC.shared().setMobileRTCRootController(self.navigationController)
//
//        /// Notification that is used to start a meeting upon log in success.
//        NotificationCenter.default.addObserver(self, selector: #selector(userLoggedIn), name: NSNotification.Name(rawValue: "userLoggedIn"), object: nil)

//        presentJoinMeetingAlert()
//        "zoom_meeting_id" = 84406716919;
//        "zoom_password" = 2403;

    }
    
    // Observe value
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let key = change?[NSKeyValueChangeKey.newKey] {
            print("observeValue \(key)") // url value
        }
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Started to load")
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Finished loading")
    }

    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }

    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping () -> Void)
    {
        let stringResult = message.contains("redirect:")
        if stringResult == true
        {
            completionHandler()
            
            let Str_messgae = message
            let defaults1 = UserDefaults.standard
            let Str_Homepath = defaults1.object(forKey: "home_path")as! String
            let Str_Api_Homepath = Str_messgae.replacingOccurrences(of: "redirect:", with: Str_Homepath)
            let Str_Final_Url = String(format: "%@?auth_token=%@", Str_Api_Homepath, Str_Encodestring)

            WebView.customUserAgent = "iOSAPP"
            let url = URL(string: Str_Final_Url)
            let request = URLRequest(url: url!)
            WebView.load(request)
        }

    }


    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping (Bool) -> Void)
    {
        print(message)

        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .actionSheet)

        alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
            completionHandler(true)
        }))

        alertController.addAction(UIAlertAction(title: "No", style: .default, handler: { (action) in
            completionHandler(false)
        }))

        present(alertController, animated: true, completion: nil)
    }


    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping (String?) -> Void) {

        let alertController = UIAlertController(title: nil, message: prompt, preferredStyle: .actionSheet)

        alertController.addTextField { (textField) in
            textField.text = defaultText
        }

        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            if let text = alertController.textFields?.first?.text {
                completionHandler(text)
            } else {
                completionHandler(defaultText)
            }
        }))

        alertController.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
            completionHandler(nil)
        }))

        present(alertController, animated: true, completion: nil)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.Label_Array.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeViewCell", for: indexPath) as! HomeViewCell
       
        cell.Lbl_Name.text = self.Label_Array[indexPath.row] as? String

        var Str_Imagename = self.Icon_Array[indexPath.row] as? String
        let stringResult = Str_Imagename?.contains("res")
        if stringResult == true
        {
            let wordToRemove = "res/"
            if let range = Str_Imagename?.range(of: wordToRemove)
            {
                Str_Imagename?.removeSubrange(range)
            }

            if let myImage = UIImage(named: Str_Imagename!)
            {
                cell.Imageview_Icon.image = myImage
            }
            else
            {
                cell.Imageview_Icon.image = UIImage(named: "more.png")
            }
        }
        else
        {
            cell.Imageview_Icon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.Imageview_Icon.sd_setImage(with: URL(string: Str_Imagename!), placeholderImage: nil)
        }



        cell.View_Round.layer.cornerRadius = 10; // this value vary as per your desire
        cell.View_Round.clipsToBounds = true;


        
        return cell

    }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let noOfCellsInRow = 3

        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout

        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))

        return CGSize(width: size, height: 128)
    }



    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let detail = storyboard?.instantiateViewController(withIdentifier: "WebRegisterView") as! WebRegisterView
        let defaults1 = UserDefaults.standard
        let Str_Homepath = defaults1.object(forKey: "home_path")
        let Str_SchoolID = defaults1.object(forKey: "school_id")
        let Str_Route = self.Route_Array[indexPath.row] as! String

        
        let Str_UserType = defaults1.object(forKey: "USER_ID")
        let Str_UserTokenExpire = defaults1.object(forKey: "USER_TOKEN")
        let Str_DecodeStr = String(format: "%@:%@", Str_UserType! as! CVarArg, Str_UserTokenExpire! as! CVarArg)
        let utf8str = Str_DecodeStr.data(using: .utf8)
        var Str_Encodestring = ""
        if let base64Encoded = utf8str?.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
        {
            Str_Encodestring = base64Encoded
            print("Encoded: \(base64Encoded)")
        }
        
        detail.Str_Urlpath = String(format: "%@%@?auth_token=%@", Str_Homepath as! CVarArg, Str_Route, Str_Encodestring)
        detail.Str_School_ID = Str_SchoolID as! String
        detail.Str_Authtoken = Str_Encodestring
        detail.Str_Title = self.Label_Array[indexPath.row] as! String
        detail.Str_CheckCookies = "2"
        self.navigationController?.pushViewController(detail, animated: true)

    }

    private func GetProfileInfo_Data()
    {
        let defaults1 = UserDefaults.standard
        let Str_UserToken = defaults1.object(forKey: "USER_TOKEN")
        let Str_UserID = defaults1.object(forKey: "USER_ID")


        let parameters = ["token": Str_UserToken, "userid": Str_UserID]


        APIManager.shared.GetProfileData(parameters:parameters as NSDictionary, onSuccess: { [self] (data) in

           if data=="auth_failed"
           {
            
           }
           else
           {
            let fullNameArr = data.split(separator:"|")
            
            let Username = String(fullNameArr[0])
            let UserImage = String(fullNameArr[1])
            
            UserDefaults.standard.set(Username, forKey: "Username")
            UserDefaults.standard.set(UserImage, forKey: "UserImage")

            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Sidemenu_Select"), object: nil)
            
            if Str_CheckLoader == "1"
            {
                self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
                navigationController?.navigationBar.titleTextAttributes = [
                    NSAttributedString.Key.foregroundColor: UIColor.white
                ]
                navigationController?.isNavigationBarHidden = false
                navigationController?.navigationBar.tintColor = UIColor.white
                let ver = UIDevice.current.systemVersion.components(separatedBy: ".")
                if Int(ver[0]) ?? 0 >= 7 {
                    navigationController?.navigationBar.barTintColor = UIColor(red: 0.13, green: 0.59, blue: 0.95, alpha: 1.00)
                    navigationController?.navigationBar.isTranslucent = false
                } else {
                    navigationController?.navigationBar.tintColor = UIColor(red: 0.13, green: 0.59, blue: 0.95, alpha: 1.00)
                }
                self.navigationItem.setHidesBackButton(true, animated: true)

                View_Gif.isHidden = true
            }
            else
            {
                _ = Timer.scheduledTimer(withTimeInterval: 3, repeats: false) { timer in
                    self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
                    navigationController?.navigationBar.titleTextAttributes = [
                        NSAttributedString.Key.foregroundColor: UIColor.white
                    ]
                    navigationController?.isNavigationBarHidden = false
                    navigationController?.navigationBar.tintColor = UIColor.white
                    let ver = UIDevice.current.systemVersion.components(separatedBy: ".")
                    if Int(ver[0]) ?? 0 >= 7 {
                        navigationController?.navigationBar.barTintColor = UIColor(red: 0.13, green: 0.59, blue: 0.95, alpha: 1.00)
                        navigationController?.navigationBar.isTranslucent = false
                    } else {
                        navigationController?.navigationBar.tintColor = UIColor(red: 0.13, green: 0.59, blue: 0.95, alpha: 1.00)
                    }
                    self.navigationItem.setHidesBackButton(true, animated: true)

                    View_Gif.isHidden = true
                }
            }
            
           }
        }) { (error) in
        }
    }

    @objc func GetApiToken_Data(notification: NSNotification)
    {
        let defaults1 = UserDefaults.standard
        let Str_UserToken = defaults1.object(forKey: "ValidateToken")
        

        let parameters = ["token": Str_UserToken]


        APIManager.shared.GetValidateToken(parameters:parameters as NSDictionary, onSuccess: { [self] (data) in
            
            if data=="auth_failed"
            {
            }
            else
            {
                print(data)

                
                if data == "valid"
                {
                    
                }
                else
                {
                    UserDefaults.standard.set("", forKey: "USER_TYPE")
                    UserDefaults.standard.set("", forKey: "USER_ID")
                    UserDefaults.standard.set("", forKey: "USER_TOKEN_EXPIRES")
                    UserDefaults.standard.set("", forKey: "USER_TOKEN")
                    UserDefaults.standard.set("", forKey: "ValidateToken")
                    
                    let detail = storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                    
                    self.navigationController?.pushViewController(detail, animated: true)
                }
                
                
            }

        }) { (error) in
        }
    }
    
    private func GetApiToken_Data1()
    {
        let defaults1 = UserDefaults.standard
        let Str_UserToken = defaults1.object(forKey: "ValidateToken")
        
        let parameters = ["token": Str_UserToken]

        APIManager.shared.GetValidateToken(parameters:parameters as NSDictionary, onSuccess: { [self] (data) in
            
            if data=="auth_failed"
            {
            }
            else
            {
                print(data)

                
                if data == "valid"
                {
                    
                }
                else
                {
                    UserDefaults.standard.set("", forKey: "USER_TYPE")
                    UserDefaults.standard.set("", forKey: "USER_ID")
                    UserDefaults.standard.set("", forKey: "USER_TOKEN_EXPIRES")
                    UserDefaults.standard.set("", forKey: "USER_TOKEN")
                    UserDefaults.standard.set("", forKey: "ValidateToken")
                    
                    let detail = storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                    detail.Str_CheckLoader = "2"
                    self.navigationController?.pushViewController(detail, animated: true)
                }
                
            }

        }) { (error) in
        }
    }

    @objc func Logout(notification: NSNotification)
    {
        let detail = storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        detail.Str_CheckLoader = "2"
        self.navigationController?.pushViewController(detail, animated: false)
    }
    
    @objc func ChangePassword(notification: NSNotification)
    {
        let defaults1 = UserDefaults.standard
        let Str_Homepath = defaults1.object(forKey: "home_path")as! String

        let detail = storyboard?.instantiateViewController(withIdentifier: "WebRegisterView") as! WebRegisterView
        detail.Str_Urlpath = String(format: "%@%@", Str_Homepath, "/change-password")
        detail.Str_Title = "Change Password"
        detail.Str_CheckCookies = "1"
        self.navigationController?.pushViewController(detail, animated: true)
    }
    
}

