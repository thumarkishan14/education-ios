//
//  Constant.swift
//  Status Video
//
//  Created by Thumar Kishan on 09/02/21.
//

import Foundation
import UIKit

let UrlGetAppIDData = "https://mobileapp.eto.school/api.php?app_id=37"

let UrlGetAppToken = "/getApplicationToken"

let UrlGetProfileInfo = "/getProfileInfo"

let UrlGetValidateToken = "/validateToken"

let UrlGetZoomData = "/getMeeting"

let UrlAttandance = "/meeting-attendance-update"
