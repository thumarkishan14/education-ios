//
//  GetAppToken.swift
//  Education
//
//  Created by Thumar Kishan on 31/07/21.
//

import Foundation
import Alamofire

extension APIManager
{
    func GetAppToken( parameters:NSDictionary, onSuccess: @escaping(String) -> Void, onFailure: @escaping(Error) -> Void)
    {
        
        let str_username : String = parameters["username"] as! String // accesses the value of key "One"
        let str_password : String = parameters["password"] as! String// accesses the value of key "One"
        let str_Schoolid : String = parameters["school"] as! String// accesses the value of key "One"

        let parameters = ["username": str_username, "password": str_password, "school": str_Schoolid]
        print(parameters)
        
        let defaults1 = UserDefaults.standard
        let Str_Apihost = defaults1.object(forKey: "api_path")as! String
        let Str_ApiUrl = String(format: "%@%@",Str_Apihost, UrlGetAppToken)
        
        var request = URLRequest(url: URL(string: Str_ApiUrl)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let data = try! JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions.prettyPrinted)

        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        if let json = json {
            print(json)
        }
        request.httpBody = json!.data(using: String.Encoding.utf8.rawValue)
        let alamoRequest = Alamofire.request(request as URLRequestConvertible)
//        alamoRequest.validate(statusCode: 200..<300)
        alamoRequest.responseString { response in

            switch response.result
            {
            case .success(let token):
                print("token:- ", token)
                onSuccess(token)
                break
            case .failure(let error):
                print("error:=", error)
                onFailure(error)
                break
            }
        }

        
    }
}
