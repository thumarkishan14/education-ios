//
//  GetAppIDdata.swift
//  Education
//
//  Created by Thumar Kishan on 31/07/21.
//

import Foundation
import Alamofire

extension APIManager
{
    func GetAppIDdata( parameters:NSDictionary, onSuccess: @escaping(NSDictionary) -> Void, onFailure: @escaping(Error) -> Void)
    {

        let parameters = ["app_id": 36]

        print(parameters)
        
        
        Alamofire.request(String(format: "%@", UrlGetAppIDData), method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
           // print(response)
            
            switch(response.result)
            {
                
            case .success( _):
                
                do {
                    let json = try JSONSerialization.jsonObject(  with: response.data!, options:.allowFragments)
                    if let json = json as? NSDictionary
                    {
                        print(json)
                        
                        onSuccess(json)
                    }else{
                        print(json as! NSDictionary)
                        
                        onSuccess(json as! NSDictionary)
                    }
                   
                } catch let myJSONError {
                    print()
                      onFailure(myJSONError)
                }
            
            case .failure(let error):
                print(error)
               onFailure(error)
                break;
            }
        }
    }
}
