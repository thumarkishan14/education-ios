//
//  GetMeetingAttandance.swift
//  Education
//
//  Created by Thumar Kishan on 14/08/21.
//

import Foundation
import Alamofire

extension APIManager
{
    func GetMeetingAttandanceData( parameters:NSDictionary, onSuccess: @escaping(NSDictionary) -> Void, onFailure: @escaping(Error) -> Void)
    {
        let str_meeting_id : String = parameters["meeting"] as! String // accesses the value of key "One"
        let str_token : String = parameters["user"] as! String// accesses the value of key "One"



        let parameters = ["meeting": str_meeting_id, "user": str_token]

        print(parameters)
        
        let defaults1 = UserDefaults.standard
        let Str_Apihost = defaults1.object(forKey: "home_path")as! String
        let Str_ApiUrl = String(format: "%@%@",Str_Apihost, UrlAttandance)
        print(Str_ApiUrl)
        
        Alamofire.request(String(format: "%@", Str_ApiUrl), method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
           // print(response)
            
            switch(response.result)
            {
                
            case .success( _):
                
                do {
                    let json = try JSONSerialization.jsonObject(  with: response.data!, options:.allowFragments)
                    if let json = json as? NSDictionary
                    {
//                        print(json)
//
//                        onSuccess(json)
                    }else{
//                        print(json as! NSDictionary)
//
//                        onSuccess(json as! NSDictionary)
                    }
                   
                } catch let myJSONError {
                    print()
                      onFailure(myJSONError)
                }
            
            case .failure(let error):
                print(error)
               onFailure(error)
                break;
            }
        }
    }
}
