//
//  GetZoomMeeting.swift
//  Education
//
//  Created by Thumar Kishan on 04/08/21.
//

import Foundation
import Alamofire

extension APIManager
{
    func GetZoomMeetingData( parameters:NSDictionary, onSuccess: @escaping(NSDictionary) -> Void, onFailure: @escaping(Error) -> Void)
    {
        let str_meeting_id : String = parameters["meeting_id"] as! String // accesses the value of key "One"
        let str_token : String = parameters["token"] as! String// accesses the value of key "One"



        let parameters = ["meeting_id": str_meeting_id, "token": str_token]

        print(parameters)
        
        let defaults1 = UserDefaults.standard
        let Str_Apihost = defaults1.object(forKey: "api_path")as! String
        let Str_ApiUrl = String(format: "%@%@",Str_Apihost, UrlGetZoomData)
        
        Alamofire.request(String(format: "%@", Str_ApiUrl), method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: nil).responseJSON { (response) in
           // print(response)
            
            switch(response.result)
            {
                
            case .success( _):
                
                do {
                    let json = try JSONSerialization.jsonObject(  with: response.data!, options:.allowFragments)
                    if let json = json as? NSDictionary
                    {
                        print(json)
                        
                        onSuccess(json)
                    }else{
                        print(json as! NSDictionary)
                        
                        onSuccess(json as! NSDictionary)
                    }
                   
                } catch let myJSONError {
                    print()
                      onFailure(myJSONError)
                }
            
            case .failure(let error):
                print(error)
               onFailure(error)
                break;
            }
        }
    }
}
